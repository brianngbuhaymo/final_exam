from __future__ import unicode_literals

from django.apps import AppConfig


class PayrollReportConfig(AppConfig):
    name = 'payroll_report'
