from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.generic import View
from login.models import Detail
from payroll_report.models import RateExpense
from datetime import datetime
from django.db.models import Sum
from django.contrib.auth.models import User

# Create your views here.
class TimeInDataView(View):
	template_name='timein_data.html'

	def get(self, request, *args, **kwargs):
		user = request.user
		detail = Detail.objects.filter(user=user).exclude(timeout__isnull=True)
		total_hours = Detail.objects.aggregate(Sum('hours'))

		return render(request, self.template_name, {'user': user, 'details': detail, 'total': total_hours})

class PayslipView(View):
	template_name='payslip.html'

	def get(self, request, *args, **kwargs):
		user = request.user
		detail = Detail.objects.filter(user=user).exclude(timeout__isnull=True)
		total_hours = Detail.objects.aggregate(Sum('hours'))

		rateexpense = RateExpense.objects.latest('name')

		total_gross = total_hours['hours__sum'] * rateexpense.hourly_rate
		total_expense =  rateexpense.withholding_tax + rateexpense.hdmf + rateexpense.phic + rateexpense.sss
		net_income = total_gross - total_expense
		return render(request, self.template_name, {
			'user': user,
			'rateexpense': rateexpense,
			'total_hours': total_hours,
			'total_expense' : total_expense,
			'total_gross': total_gross,
			'net_income' : net_income,
		})

class PayrollReportView(View):
	template_name='payroll_report.html'

	def get(self, request, *args, **kwargs):
		user = request.user
		detail = Detail.objects.filter(user=user).exclude(timeout__isnull=True)
		total_hours = Detail.objects.aggregate(Sum('hours'))

		rateexpense = RateExpense.objects.latest('name')

		total_gross = total_hours['hours__sum'] * rateexpense.hourly_rate
		total_expense =  rateexpense.withholding_tax + rateexpense.hdmf + rateexpense.phic + rateexpense.sss
		net_income = total_gross - total_expense
		return render(request, self.template_name)

class TimeInReportView(View):
	template_name='timein_report.html'

	def get(self, request, *args, **kwargs):
		user = request.user
		detail = Detail.objects.filter(user=user).exclude(timeout__isnull=True)
		total_hours = Detail.objects.aggregate(Sum('hours'))

		rateexpense = RateExpense.objects.latest('name')

		total_gross = total_hours['hours__sum'] * rateexpense.hourly_rate
		total_expense =  rateexpense.withholding_tax + rateexpense.hdmf + rateexpense.phic + rateexpense.sss
		net_income = total_gross - total_expense
		return render(request, self.template_name)

class EmployeesView(View):
	template_name='employees.html'

	def get(self, request, *args, **kwargs):
		users = User.objects.all()
		
		return render(request, self.template_name, {'users': users})


timeindata_view = TimeInDataView.as_view()
payslip_view = PayslipView.as_view()
payrollreport_view = PayrollReportView.as_view()
timeinreport_view = TimeInReportView.as_view()
employees_view = EmployeesView.as_view()