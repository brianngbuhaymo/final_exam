from __future__ import unicode_literals

from django.db import models

# Create your models here.
class RateExpense(models.Model):
    name = models.CharField(max_length=100)
    hourly_rate = models.DecimalField(max_digits=10, decimal_places=2)
    withholding_tax = models.DecimalField(max_digits=20, decimal_places=2)
    hdmf = models.DecimalField(max_digits=20, decimal_places=2)
    phic = models.DecimalField(max_digits=20, decimal_places=2)
    sss = models.DecimalField(max_digits=20, decimal_places=2)

    def __unicode__(self):
        return self.name