"""payroll URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from login import views as login
from payroll_report import views as main
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/$', login.login_view, name='login'),
    url(r'^logout/$', login_required(login.logout_view), name='logout'),
    url(r'^welcome/$', login_required(login.welcome_view), name='welcome'),
    url(r'^settings/$', login_required(login.settings_view), name='settings'),
    url(r'^my_hours/$', login_required(main.timeindata_view), name='my_hours'),
    url(r'^payslip/$', login_required(main.payslip_view), name='payslip'),
    url(r'^employees/$', login_required(main.employees_view), name='employees'),
    url(r'^time_in_report/$', login_required(main.timeinreport_view), name='time_in_report'),
    url(r'^payroll_report/$', login_required(main.payrollreport_view), name='payroll_report'),
]
