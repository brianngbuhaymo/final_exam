from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.generic import View
from .models import Detail
from datetime import datetime
from django.contrib.auth import authenticate, login, logout
from django.db.models import Sum

from .forms import LoginForm, ChangeUsernameForm, ChangePasswordForm

# Create your views here.
class LoginView(View):
	template_name = 'login.html'
	form = LoginForm

	def get(self, request, *args, **kwargs):
		form = self.form()
		return render(request, self.template_name, {'form': form})

	def post(self, request, *args, **kwargs):
		form = self.form(request.POST)
		if form.is_valid():
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			user = authenticate(username=username, password=password)
			login_time = datetime.now().time()
			login_date = datetime.now().date()

			if user is not None:
				login(request, user)

				user_timein = Detail(user=request.user, date=login_date, timein=login_time)
				user_timein.save()
				return redirect('welcome')

		return render(request,self.template_name, {'form': form})

class LogoutView(View):
	template_name = 'logout.html'

	def get(self, request, *args, **kwargs):
		user = request.user
		logout_time = datetime.now().time()
		user_timeout = Detail.objects.latest('timein')
		user_timeout.timeout = logout_time
		user_timeout.hours = ((datetime.combine(user_timeout.date, user_timeout.timeout) - datetime.combine(user_timeout.date, user_timeout.timein)).total_seconds()) / 3600
		user_timeout.save()
		logout(request)
		return render(request, self.template_name, {'user': user, 'detail': user_timeout})

	def post(self, request, *args, **kwargs):
		logout(request)
		return redirect('login')
		
class WelcomeView(View):
	template_name='welcome.html'

	def get(self, request, *args, **kwargs):
		user = request.user
		detail = Detail.objects.latest('timein')
		return render(request, self.template_name, {'user': user, 'detail': detail})

class SettingsView(View):
	template_name = 'settings.html'
	form1 = ChangeUsernameForm
	form2 = ChangePasswordForm

	def get(self, request, *args, **kwargs):
		form1 = self.form1()
		form2 = self.form2()
		return render(request, self.template_name, {'form1': form1, 'form2': form2})

	def post(self, request, *args, **kwargs):
		form1 = self.form1(request.POST)
		if form.is_valid():
			return render(request, self.template_name, {'form1': form1, 'form2': form2})

		return render(request, self.template_name, {'form1': form1, 'form2': form2})


login_view = LoginView.as_view()
logout_view = LogoutView.as_view()
welcome_view = WelcomeView.as_view()
settings_view = SettingsView.as_view()