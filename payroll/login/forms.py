from django import forms
from .models import User

class LoginForm(forms.Form):
    username = forms.CharField(max_length=50)
    password = forms.CharField(widget=forms.PasswordInput())

class ChangeUsernameForm(forms.Form):
    new_username = forms.CharField(max_length=50)
    password = forms.CharField(widget=forms.PasswordInput())

class ChangePasswordForm(forms.Form):
    username = forms.CharField(max_length=50)
    password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())