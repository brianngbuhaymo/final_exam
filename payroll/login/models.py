from __future__ import unicode_literals
import datetime
from django.db import models
from django.contrib.auth.models import User

# Create your models here.

optional = {
    'null': True,
    'blank' : True
}

class Detail(models.Model):
    user = models.ForeignKey(User)
    department = models.CharField(max_length=50, **optional)
    date = models.DateField(**optional)
    timein = models.TimeField()
    timeout = models.TimeField(**optional)
    hours = models.DecimalField(max_digits=4, decimal_places=2, **optional)
    
    def __unicode__(self):
        return unicode(self.user)

